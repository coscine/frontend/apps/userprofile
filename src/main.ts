import jQuery from "jquery";
import Vue from "vue";
import UserProfile from "./UserProfile.vue";
import VueI18n from "vue-i18n";
import { LanguageUtil } from "@coscine/app-util";
import { BootstrapVue, BSkeleton } from "bootstrap-vue";
import "@voerro/vue-tagsinput/dist/style.css";

Vue.config.productionTip = false;
Vue.use(VueI18n);
Vue.use(BootstrapVue);
Vue.component("b-skeleton", BSkeleton);
Vue.component("BIconClipboard", BSkeleton);

jQuery(() => {
  const i18n = new VueI18n({
    locale: LanguageUtil.getLanguage(),
    messages: coscine.i18n.userprofile,
    silentFallbackWarn: true,
  });

  new Vue({
    render: (h) => h(UserProfile),
    i18n,
  }).$mount("userprofile");
});
