declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "@voerro/vue-tagsinput";
declare module "@coscine/project-creation";
declare module "@coscine/app-util";
declare module "@coscine/component-library";
declare module "moment";
declare module "vue-loading-overlay";
declare module "vue-multiselect";
declare module "vuejs-datepicker";
declare module "vuejs-datepicker/dist/locale";

declare module "*.png" {
  const value: string;
  export default value;
}
